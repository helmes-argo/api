<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSectorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_sector', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('sector_id')->unsigned();

            $table->foreign('user_id', 'fk_us_user_id')->references('id')->on('user')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('sector_id', 'fk_us_sector_id')->references('id')->on('sector')->onDelete('restrict')->onUpdate('cascade');
            $table->primary(['user_id', 'sector_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_sector', function (Blueprint $table) {
            $table->dropForeign('fk_us_user_id');
            $table->dropForeign('fk_us_sector_id');
        });
        Schema::dropIfExists('user_sector');
    }
}
