<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSectorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sector', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('name', 255);
            $table->string('description', 100)->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->unique(['parent_id', 'name']);

            $table->foreign('parent_id', 'fk_sector_parent_id')->references('id')->on('sector')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sector', function (Blueprint $table) {
            $table->dropForeign('fk_sector_parent_id');
        });
        Schema::dropIfExists('sector');
    }
}
