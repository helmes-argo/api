<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sectorIds = \App\Models\Sector::pluck('id');
        for ($i = 0; $i < 10; $i++) {
            $user = factory(App\Models\User::class)->create([
                'email' => 'test' . $i . '@test.ee',
                'password' => app('hash')->make('test123'),
            ]);
            $sectors = $sectorIds->shuffle()->take(rand($sectorIds->first(), min($sectorIds->last(), 10)))->toArray();
            $user->sectors()->sync($sectors);

        }

    }
}
