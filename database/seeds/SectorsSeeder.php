<?php

use Illuminate\Database\Seeder;

class SectorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sectors = [
            [
                'name' => 'Manufacturing',
                'subs' => [
                    [
                        'name' => 'Construction materials',
                    ],
                    [
                        'name' => 'Electronics and Optics',
                    ],
                    [
                        'name' => 'Food and Beverage',
                        'subs' => [
                            [
                                'name' => 'Bakery & confectionery products'
                            ],
                            [
                                'name' => 'Beverages'
                            ],
                            [
                                'name' => 'Fish & fish products'
                            ],
                            [
                                'name' => 'Meat & meat products'
                            ],
                            [
                                'name' => 'Milk & dairy products'
                            ],
                            [
                                'name' => 'Other'
                            ],
                            [
                                'name' => 'Sweets & snack food'
                            ],
                        ],
                    ],
                    [
                        'name' => 'Furniture',
                        'subs' => [
                            [
                                'name' => 'Bathroom/sauna',
                            ],
                            [
                                'name' => 'Bedroom',
                            ],
                            [
                                'name' => 'Children’s room',
                            ],
                            [
                                'name' => 'Kitchen',
                            ],
                            [
                                'name' => 'Living room',
                            ],
                            [
                                'name' => 'Office',
                            ],
                            [
                                'name' => 'Other (Furniture)',
                            ],
                            [
                                'name' => 'Outdoor',
                            ],
                            [
                                'name' => 'Project furniture',
                            ],
                        ],
                    ],
                    [
                        'name' => 'Machinery',
                        'subs' => [
                            [
                                'name' => 'Machinery components',
                            ],
                            [
                                'name' => 'Machinery equipment/tools',
                            ],
                            [
                                'name' => 'CManufacture of machinery',
                            ],
                            [
                                'name' => 'Maritime',
                                'subs' => [
                                    [
                                        'name' => 'Aluminium and steel workboats',
                                    ],
                                    [
                                        'name' => 'Boat/Yacht building',
                                    ],
                                    [
                                        'name' => 'Ship repair and conversion',
                                    ]
                                ]
                            ],
                            [
                                'name' => 'Metal structures',
                            ],
                            [
                                'name' => 'Other',
                            ],
                            [
                                'name' => 'Repair and maintenance service',
                            ],
                        ],
                    ],
                    [
                        'name' => 'Metalworking',
                        'subs' => [
                            [
                                'name' => 'Construction of metal structures',
                            ],
                            [
                                'name' => 'Houses and buildings',
                            ],
                            [
                                'name' => 'Metal products',
                            ],
                            [
                                'name' => 'Metal works',
                                'subs' => [
                                    [
                                        'name' => 'CNC-machining',
                                    ],
                                    [
                                        'name' => 'Forgings, Fasteners',
                                    ],
                                    [
                                        'name' => 'Gas, Plasma, Laser cutting',
                                    ],
                                    [
                                        'name' => 'MIG, TIG, Aluminum welding',
                                    ],
                                ]
                            ],
                        ],
                    ],
                    [
                        'name' => 'Plastic and Rubber',
                        'subs' => [
                            [
                                'name' => 'Packaging',
                            ],
                            [
                                'name' => 'Plastic goods',
                            ],
                            [
                                'name' => 'Plastic processing technology',
                                'subs' => [
                                    [
                                        'name' => 'Blowing',
                                    ],
                                    [
                                        'name' => 'Moulding',
                                    ],
                                    [
                                        'name' => 'Plastics welding and processing',
                                    ],
                                ]
                            ],
                            [
                                'name' => 'Plastic profiles',
                            ],
                        ],
                    ],
                    [
                        'name' => 'Printing',
                        'subs' => [
                            [
                                'name' => 'Advertising',
                            ],
                            [
                                'name' => 'Book/Periodicals printing',
                            ],
                            [
                                'name' => 'Labelling and packaging printing',
                            ],

                        ]
                    ],
                    [
                        'name' => 'Textile and Clothing',
                        'subs' => [
                            [
                                'name' => 'Clothing',
                            ],
                            [
                                'name' => 'Textile',
                            ],
                        ]
                    ],
                    [
                        'name' => 'Wood',
                        'subs' => [
                            [
                                'name' => 'Other (Wood)',
                            ],
                            [
                                'name' => 'Wooden building materials',
                            ],
                            [
                                'name' => 'Wooden houses',
                            ],
                        ]
                    ],
                ],
            ],
            [
                'name' => 'Other',
                'subs' => [
                    [
                        'name' => 'Creative industries',
                    ],
                    [
                        'name' => 'Energy technology',
                    ],
                    [
                        'name' => 'Environment',
                    ],
                ]
            ],
            [
                'name' => 'Service',
                'subs' => [
                    [
                        'name' => 'Business services',
                    ],
                    [
                        'name' => 'Engineering',
                    ],
                    [
                        'name' => 'Information Technology and Telecommunications',
                        'subs' => [
                            [
                                'name' => 'Data processing, Web portals, E-marketing',
                            ],
                            [
                                'name' => 'Programming, Consultancy',
                            ],
                            [
                                'name' => 'Software, Hardware',
                            ],
                            [
                                'name' => 'Telecommunications',
                            ],
                        ]
                    ],
                    [
                        'name' => 'Tourism',
                    ],
                    [
                        'name' => 'Translation services',
                    ],
                    [
                        'name' => 'Transport and Logistics',
                        'subs' => [
                            [
                                'name' => 'Air',
                            ],
                            [
                                'name' => 'Rail',
                            ],
                            [
                                'name' => 'Road',
                            ],
                            [
                                'name' => 'Water',
                            ],
                        ],
                    ],
                ]
            ]



        ];

        $this->createSector($sectors);

    }

    protected function createSector(array $sectors, int $parentId = null)
    {
        foreach ($sectors as $sectorData) {
            $sector = factory(App\Models\Sector::class)->create([
                'name' => $sectorData['name'],
                'parent_id' => $parentId,
            ]);
            if (isset($sectorData['subs']) && !empty($sectorData['subs'])) {
                $this->createSector($sectorData['subs'], $sector->id);
            }
        }
    }
}
