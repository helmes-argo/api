<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'v1'], function() use ($router) {
    $router->post('/login', 'AuthController@login');

    $router->get('/sectors', 'SectorController@index');
    $router->get('/users', 'UserController@index');
    $router->get('/users/{id}', 'UserController@show');
    $router->post('/users', 'UserController@store');

    // Protect these routes with jwt
    $router->group(['middleware' => 'jwt.auth'], function() use ($router) {
        $router->put('/users/{id}', 'UserController@update');
    });

});
