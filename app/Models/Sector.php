<?php

namespace App\Models;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sector extends Model
{
    use SoftDeletes;

    protected $table = 'sector';

    protected $subsectors;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'parent_id',
        'description',
    ];


    public static function formatHierarchically(Collection $sectors, int $parentId = null)
    {
        $sectors = $sectors->map(function($sector) use ($sectors) {
            $sector->subsectors = $sectors->filter(function($subsector) use ($sector) {
                return $subsector->parent_id == $sector->id;
            });
            return $sector;
        });
        return $sectors;
    }

    public function subsectors()
    {
        return $this->hasMany('App\Models\Sector', 'parent_id', 'id');

    }

}
