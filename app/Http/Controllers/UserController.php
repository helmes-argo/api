<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class UserController extends Controller
{

    /**
     * Show all users
     *
     * @param  \Illuminate\Support\Request $request
     *
     * @return \Illuminate\Support\Response
     */
    public function index(Request $request)
    {
        return $this->showAll(User::with('sectors')->get());
    }

    /**
     * Show one user
     *
     * @param  \Illuminate\Support\Request $request
     *
     * @return \Illuminate\Support\Response
     */
    public function show(Request $request, int $id)
    {
        return $this->showOne(User::with('sectors')->firstOrFail($id));
    }

    /**
     * Store user
     *
     * @param  \Illuminate\Support\Request $request
     *
     * @return \Illuminate\Support\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'first_name' => 'nullable|string|max:50',
            'last_name' => 'nullable|string|max:50',
            'email' => 'sometimes|required|email|max:100',
            'password' => 'sometimes|required',
            'sectors' => 'sometimes|required|array',
            'sectors.*' => 'sometimes|required|integer',
            'agreed_to_terms' => 'sometimes|required',
        ];

        $this->validate($request, $rules);

        $attributes = $request->only(['first_name', 'last_name', 'email', 'password', 'agreed_to_terms']);

        if ($request->has('password')) {
            $attributes['password'] = app('hash')->make($request->password);
        }

        $user = User::create($attributes);

        if ($request->has('sectors')) {
            $user->sectors()->sync($request->sectors);
        }

        return $this->showOne($user->with('sectors')->find($user->id));
    }

    /**
     * Update user
     *
     * @param  \Illuminate\Support\Request $request
     *
     * @return \Illuminate\Support\Response
     */
    public function update(Request $request, int $id)
    {
        $rules = [
            'first_name' => 'nullable|string|max:50',
            'last_name' => 'nullable|string|max:50',
            'email' => 'sometimes|required|email|max:100',
            'password' => 'sometimes|required',
            'sectors' => 'sometimes|required|array',
            'sectors.*' => 'sometimes|required|integer',
        ];

        $this->validate($request, $rules);
        $user = User::findOrFail($id);

        $user->fill($request->only(['first_name', 'last_name', 'email', 'password']));

        if ($user->isClean()) {
            return $this->errorResponse([ 'error' => 'Nothing was modified' ], Response::HTTP_BAD_REQUEST);
        }

        if ($request->has('sectors')) {
            $user->sectors()->sync($request->sectors);
        }
        $user->save();

        return $this->showOne($user->with('sectors')->find($user->id));
    }


}
