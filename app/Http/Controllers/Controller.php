<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Response;

class Controller extends BaseController
{
    /**
     * Parse and output a collection of a model in response
     *
     * @param \Illuminate\Support\Collection $data
     * @param int|null $responseCode
     */
    protected function showAll(Collection $data, int $responseCode = null)
    {
        return $this->successResponse($data, $responseCode);
    }

    /**
     * Parse and output one model instance in response
     * @param \Illuminate\Database\Eloquent\Model $data
     * @param int|null $responseCode
     */
    protected function showOne(Model $data, int $responseCode = null)
    {
        return $this->successResponse($data, $responseCode);
    }

    /**
     * Return a json response with a success code
     *
     * @param mixed $data
     * @param int $responseCode
     *
     * @return \Illuminate\Http\Response
     */
    protected function successResponse($data, int $responseCode = null)
    {
        return response()->json($data, $responseCode ?? Response::HTTP_OK);
    }

    /**
     * Return a json response with error code
     *
     * @param mixed $data
     * @param int $responseCode
     *
     * @return \Illuminate\Http\Response
     */
    protected function errorResponse($data, int $responseCode = null)
    {
        return response()->json($data, $responseCode ?? Response::HTTP_BAD_REQUEST);
    }
}
