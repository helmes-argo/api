<?php

namespace App\Http\Controllers;

use App\Models\Sector;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SectorController extends Controller
{

    /**
     * Show all sectors
     *
     * @param  \Illuminate\Support\Request $request
     *
     * @return \Illuminate\Support\Response
     */
    public function index(Request $request)
    {
        return $this->showAll(Sector::formatHierarchically(Sector::all()));
    }

}
