<?php

namespace App\Http\Controllers;

use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    /**
     * Create a new token.
     *
     * @param  \App\Models\User $user
     * @return string
     */
    protected function generateToken(User $user)
    {
        $payload = [
            'issuer' => "lumen-jwt", // Issuer of the token
            'userId' => $user->id, // Subject of the token
            'timeIssued' => time(), // Time when JWT was issued.
            'expiry' => time() + config('auth.token_expiry') // Expiration time
        ];

        return JWT::encode($payload, config('auth.jwt_secret'));
    }

    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function login(Request $request, User $user)
    {
        $this->validate($request, [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);

        // Find the user by email
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return response()->json([
                'error' => 'Wrong email address'
            ], Response::HTTP_NOT_FOUND);
        }

        // Verify the password and generate the token
        if (Hash::check($request->password, $user->password)) {
            return $this->successResponse([
                'token' => $this->generateToken($user),
                'user' => $user->toArray(),
            ]);
        }

        // Bad Request response
        return $this->errorResponse([
            'error' => 'Email or password is wrong.'
        ], 400);
    }
}
