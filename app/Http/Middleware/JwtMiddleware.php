<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use App\Models\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class JwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('jwt-token');

        if(!$token) {
            return response()->json([
                'error' => 'Unauthorized.'
            ], Response::HTTP_UNAUTHORIZED);
        }
        try {
            $credentials = JWT::decode($token, config('auth.jwt_secret'), ['HS256']);
        } catch(ExpiredException $e) {
            return response()->json([
                'error' => 'Provided token is expired.'
            ], Response::HTTP_BAD_REQUEST);
        } catch(Exception $e) {
            Log::error('Unexpected error during auth check', ['trace' => $e->getTraceAsString()]);
            return response()->json([
                'error' => 'An error while decoding token.'
            ], Response::HTTP_BAD_REQUEST);
        }

        $user = User::findOrFail($credentials->userId);
        if ($user) {
            $request->auth = $user; // adding user to the request for global access, otherwise anonymous
        }


        return $next($request);
    }
}
