<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class CorsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $headers = [
            'Access-Control-Allow-Headers' => config('cors.allowed_headers'),
            'Access-Control-Allow-Origin' => config('cors.allowed_origins'),
            'Access-Control-Allow-Methods' => config('cors.allowed_methods'),
            'Access-Control-Allow-Credentials' => 'true',
            'Access-Control-Max-Age' => '86400',
        ];

        if ($request->isMethod('OPTIONS')) {
           return response()->json([], Response::HTTP_OK, $headers);
        }
        $response = $next($request);

        foreach($headers as $key => $value)
        {
            $response->header($key, $value);
        }
        return $response;
    }
}
