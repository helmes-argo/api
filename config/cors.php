<?php

return [
    'allowed_headers' => env('CORS_ALLOWED_HEADERS', '*'),
    'allowed_methods' => env('CORS_ALLOWED_METHODS', 'GET,PUT,POST'),
    'allowed_origins' => env('CORS_ALLOWED_ORIGINS', '*'),
];