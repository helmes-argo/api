<?php

return [
    'token_expiry' => env('TOKEN_EXPIRY', 3600),
    'jwt_secret' => env('JWT_SECRET', ''),
];